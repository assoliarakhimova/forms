import {FormInput} from './forminput';
import { Controller, useForm, useWatch } from 'react-hook-form';
import React from 'react';
import "./event.css"


export type SignUpForm = {
    eventname: string;
    description: string;
    img: string;
    location:string;  
    category:string;
    date:string;
    time:string;

  };

type Props = {
  initialValues?: SignUpForm;
  isLoading: boolean;
  onSubmit: (data: SignUpForm) => void;
};

const defaultValues: SignUpForm  = {
  eventname: "",
  description: "",
  img:"" ,
  location:"", 
  category:"",
  date:new Date().toString(),
  time:new Date().getHours.toString(),
};

export const PostForm: React.FC<Props> = ({ initialValues, isLoading, onSubmit }) => {
  const {
    register,
    control,
    handleSubmit,
    reset,
    formState:{ errors , isValid } 
  } = useForm<SignUpForm>({defaultValues ,mode:"all"} );


  const handleSuccess = (values: SignUpForm) => {
    onSubmit(values);
    reset();
  };

  return (
    <div className ="back">
    <form onSubmit={handleSubmit(handleSuccess)}>
      <p>Мероприятие</p>
      <Controller
        control={control}
        name="eventname"
  
        
        rules={{
          required: true,
          maxLength: {
            value: 50,
            message: "Название должно умещаться в 50 символов, можете добавить описание"
          },  pattern: {
            value: /^[A-Za-zА-Яа-я0-9/-/!/?/./]/,
            message: "Название не должно включать особые символы"
          },
          
         }} 

        render={({ field: { onChange, value, name } }) => (
          <FormInput label="Название Мероприятия" value={value} name={name} onChange={onChange} type ="text" className="event"/>
        )}
      />
      <Controller
        control={control}
        name="description"
        
        rules={{
          required: true,
          maxLength: {
            value: 500,
            message: "Максимальное количество символов-500"
          }
         }} 

        render={({ field: { onChange, value, name } }) => (
          <FormInput label="Описание" value={value} name={name} onChange={onChange} type ="textarea" className="description"/>
        )}
      />
      <div className = "pictures">
      <Controller
        control={control}
        name="img"
        
        rules={{
          required: true,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "Загрузить фотографии" value={value} name={name} onChange={onChange} type="file" className="img"  />
        )}
      />
      <Controller
        control={control}
        name="img"
        
        rules={{
          required: false,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "" value={value} name={name} onChange={onChange} type="file" className="img"  />
        )}
      />
      <Controller
        control={control}
        name="img"
        
        rules={{
          required: false,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "" value={value} name={name} onChange={onChange} type="file" className="img"  />
        )}
      />
      <Controller
        control={control}
        name="img"
        
        rules={{
          required: false,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "" value={value} name={name} onChange={onChange} type="file" className="img"  />
        )}
      />
      <Controller
        control={control}
        name="img"
        
        rules={{
          required: false,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "" value={value} name={name} onChange={onChange} type="file" className="img"  />
        )}
      />
      </div>
      <p >Выбрать категорию</p>
      <div className ="buttons"> 
       <Controller
        control={control}
        name="category"
        
        rules={{
          required: true,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "Спорт" value={value} name={name} onChange={onChange} type="radio" className="button"/>
        )}
      />
      <Controller
        control={control}
        name="category"
        
        rules={{
          required: true,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "Культурные" value={value} name={name} onChange={onChange} type="radio" className="button"/>
        )}
      />
      <Controller
        control={control}
        name="category"
        
        rules={{
          required: true,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "Образовательные" value={value} name={name} onChange={onChange} type="radio" className="button"/>
        )}
      />
      <Controller
        control={control}
        name="category"
        
        rules={{
          required: true,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "Развлекательные" value={value} name={name} onChange={onChange} type="radio" className="button"/>
        )}
      />
      </div>
      <p className='text'>Время и Место</p>
      <Controller
        control={control}
        name="location"
        
        rules={{
          required: true, 
          maxLength: {
            value: 100,
            message: "Максимальное колиество символов 100"
          }     
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "Адрес" value={value} name={name} onChange={onChange} type="text" className="address"/>
        )}
      />
      <p>Дата и время</p>  
      <div className='Day'>
      <Controller
        control={control}
        name="date"
        
        rules={{
          required: true,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label="Дата и время" value={value} name={name} onChange={onChange} type="date" className="date" placeholder='' />
        )}
      />
      <Controller
        control={control}
        name="time"
        
        rules={{
          required: true,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "" value={value} name={name} onChange={onChange} type="time" className="time" placeholder=''/>
        )}
      />
      <Controller
        control={control}
        name="time"
        
        rules={{
          required: true,      
         }} 
        render={({ field: { onChange, value, name } }) => (
          <FormInput label= "" value={value} name={name} onChange={onChange} type="time" className="time" placeholder='11:00'/>
        )}
      />
      </div>
      <button type="submit" disabled={isLoading} className="buttonSubmit">
        Создать Мероприятие
      </button>
    </form>
    </div>
  );
};
/* пока что для проверки только isLoading, я еще чуть позже добавлю инвалид */